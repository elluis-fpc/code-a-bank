/* eslint-disable no-undef */
/* eslint-disable import/extensions */
import _ from 'lodash';
import fixtures from '../fixtures/db.js';

describe('Transactions API', () => {
  let fixture;
  let transaction;
  let sourceAccount;
  let destinationAccount;

  beforeEach(() => {
    fixture = _.cloneDeep(fixtures);
    app.db.data = fixture;

    [transaction] = fixture.transactions;
    [sourceAccount, destinationAccount] = fixture.accounts;
  });

  it('GET /transaction should return a transaction', (done) => {
    request
      .get(`/transactions/${transaction.id}`)
      .send(transaction)
      .expect(200)
      .end((err, res) => {
        expect(res.body.firstName).to.eql(transaction.firstName);
        expect(res.body.lastName).to.eql(transaction.lastName);

        done(err);
      });
  });

  describe('Deposit transaction', () => {
    it('POST /transactions - Successful deposit request', (done) => {
      const newTransaction = {
        accountId: sourceAccount.id,
        type: 'deposit',
        amount: 100,
      };

      request
        .post('/transactions')
        .send(newTransaction)
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.include(newTransaction);

          done(err);
        });
    });

    it('POST /transactions - Should add amount to account', (done) => {
      const newTransaction = {
        accountId: sourceAccount.id,
        type: 'deposit',
        amount: 100,
      };

      const expectedBalance = sourceAccount.balance + newTransaction.amount;

      request
        .post('/transactions')
        .send(newTransaction)
        .expect(200)
        .end((err) => {
          expect(expectedBalance).to.eql(sourceAccount.balance);
          done(err);
        });
    });
  });

  describe('Withdrawal transaction', () => {
    it('POST /transactions - Successful withdrawal request', (done) => {
      const newTransaction = {
        accountId: sourceAccount.id,
        type: 'withdrawal',
        amount: 100,
      };

      request
        .post('/transactions')
        .send(newTransaction)
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.include(newTransaction);

          done(err);
        });
    });

    it('POST /transactions - Should subtract amount to account', (done) => {
      const newTransaction = {
        accountId: sourceAccount.id,
        type: 'withdrawal',
        amount: 25,
      };

      const expectedBalance = sourceAccount.balance - newTransaction.amount;

      request
        .post('/transactions')
        .send(newTransaction)
        .expect(200)
        .end((err) => {
          expect(expectedBalance).to.eql(sourceAccount.balance);
          done(err);
        });
    });

    it('POST /transactions - Balance cannot be below 0', (done) => {
      const newTransaction = {
        accountId: sourceAccount.id,
        type: 'withdrawal',
        amount: 100000,
      };

      const expectedBalance = 0;

      request
        .post('/transactions')
        .send(newTransaction)
        .expect(200)
        .end((err) => {
          expect(expectedBalance).to.eql(sourceAccount.balance);
          done(err);
        });
    });
  });

  describe('Transfer transaction', () => {
    it('POST /transactions - Successful transfer request', (done) => {
      const newTransaction = {
        accountId: sourceAccount.id,
        destinationAccountId: destinationAccount.id,
        type: 'transfer',
        amount: 55,
      };

      request
        .post('/transactions')
        .send(newTransaction)
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.include(newTransaction);

          done(err);
        });
    });

    it('POST /transactions - Should successfully transfer amount to destination account', (done) => {
      const newTransaction = {
        accountId: sourceAccount.id,
        destinationAccountId: destinationAccount.id,
        type: 'transfer',
        amount: 55,
      };

      const expectedSourceBalance = sourceAccount.balance - newTransaction.amount;
      const expectedDestinationBalance = destinationAccount.balance + newTransaction.amount;

      request
        .post('/transactions')
        .send(newTransaction)
        .expect(200)
        .end((err) => {
          expect(expectedSourceBalance).to.eql(sourceAccount.balance);
          expect(expectedDestinationBalance).to.eql(destinationAccount.balance);

          done(err);
        });
    });
  });

  describe('Error handling', () => {
    it('POST /transactions - Error: Invalid amount', (done) => {
      const newTransaction = {
        accountId: sourceAccount.id,
        type: 'deposit',
        amount: 'abc',
      };

      request
        .post('/transactions')
        .send(newTransaction)
        .expect(400)
        .end((err, res) => {
          expect(res.body).to.include({
            message: 'Error: Invalid amount',
          });

          done(err);
        });
    });

    it('POST /transactions - Error: Account not found', (done) => {
      const newTransaction = {
        accountId: 'DOES NOT EXIST',
        type: 'deposit',
        amount: 100,
      };

      request
        .post('/transactions')
        .send(newTransaction)
        .expect(404)
        .end((err, res) => {
          expect(res.body).to.include({
            message: 'Error: Account not found',
          });

          done(err);
        });
    });

    it('POST /transactions - Error: Invalid transaction type', (done) => {
      const newTransaction = {
        accountId: sourceAccount.id,
        type: 'INVALID',
        amount: 'abc',
      };

      request
        .post('/transactions')
        .send(newTransaction)
        .expect(400)
        .end((err, res) => {
          expect(res.body).to.include({
            message: 'Error: Invalid transaction type',
          });

          done(err);
        });
    });

    it('POST /transactions - Transfer - Error: Destination account not found', (done) => {
      const newTransaction = {
        accountId: sourceAccount.id,
        destinationAccountId: 'DOES NOT EXIST',
        type: 'transfer',
        amount: 100000,
      };

      request
        .post('/transactions')
        .send(newTransaction)
        .expect(404)
        .end((err, res) => {
          expect(res.body).to.include({
            message: 'Error: Destination account not found',
          });

          done(err);
        });
    });

    it('POST /transactions - Transfer - Error: Insufficient balance', (done) => {
      const newTransaction = {
        accountId: sourceAccount.id,
        destinationAccountId: destinationAccount.id,
        type: 'transfer',
        amount: 100000,
      };

      request
        .post('/transactions')
        .send(newTransaction)
        .expect(400)
        .end((err, res) => {
          expect(res.body).to.include({
            message: 'Error: Insufficient balance',
          });

          done(err);
        });
    });
  });
});
