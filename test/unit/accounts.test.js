/* eslint-disable no-undef */
/* eslint-disable import/extensions */
import _ from 'lodash';
import fixtures from '../fixtures/db.js';

describe('Accounts API', () => {
  let fixture;
  let user;
  let account;

  beforeEach(() => {
    fixture = _.cloneDeep(fixtures);
    app.db.data = fixture;

    [user] = fixture.users;
    [account] = fixture.accounts;
  });

  it('GET /accounts/:id - Should return account record ', (done) => {
    request
      .get(`/accounts/${account.id}`)
      .send(account)
      .expect(200)
      .end((err, res) => {
        expect(res.body).to.include(account);

        done(err);
      });
  });

  it('POST /accounts should create an account', (done) => {
    const newAccount = {
      userId: user.id,
      type: 'savings',
    };

    request
      .post('/accounts')
      .send(newAccount)
      .expect(201)
      .end((err, res) => {
        newAccount.balance = 0;

        expect(res.body).to.include(newAccount);

        done(err);
      });
  });

  describe('Error handling', () => {
    it('GET /accounts/:id - Error: Account does not exist ', (done) => {
      request
        .get('/accounts/INVALID')
        .send(account)
        .expect(404)
        .end((err, res) => {
          expect(res.body).to.include({ message: 'Error: Account does not exist' });

          done(err);
        });
    });

    it('POST /accounts - Error: Invalid account type', (done) => {
      const newAccount = {
        userId: user.id,
        type: 'INVALID',
      };

      request
        .post('/accounts')
        .send(newAccount)
        .expect(400)
        .end((err, res) => {
          expect(res.body).to.include({ message: 'Error: Invalid account type' });

          done(err);
        });
    });

    it('POST /accounts - Error: Invalid user for account', (done) => {
      const newAccount = {
        userId: 'INVALID',
        type: 'savings',
      };

      request
        .post('/accounts')
        .send(newAccount)
        .expect(400)
        .end((err, res) => {
          expect(res.body).to.include({ message: 'Error: Invalid user for account' });

          done(err);
        });
    });
  });
});
