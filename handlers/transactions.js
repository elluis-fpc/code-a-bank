import _ from 'lodash';
import { DateTime } from 'luxon';
import uuid from 'short-uuid';

export default function (app) {
  /**
     * Retrieves a Transaction record
     * @param {number} id - The transaction ID
     * @return {object} - The transaction details including balance
     */
  app.get('/transactions/:id', async (req, res) => {
    const {
      transactions,
    } = req.db.data;
    const transactionId = req.params.id;
    const transaction = transactions.find((r) => r.id === transactionId);

    if (!transaction) return res.sendStatus(404);

    return res.send(transaction);
  });

  const transferTx = (tx) => {
    const res = {
      status: true,
      msg: '',
    };

    if (tx.account.balance < tx.txAmount) {
      res.status = false;
      res.msg = 'Error: Insufficient balance';
    }

    res.srcBal = tx.account.balance - tx.txAmount;
    res.destBal = tx.destAccount.balance + tx.txAmount;

    return res;
  };

  /**
     * Transaction processing
     * @param {string} accountId  - The target transaction account
     * @param {number} amount - The transaction amount. Should be positive value.
     * @param {string} destinationAccountId  - For transfer type transaction. The destination
     *  account to transfer the amount.
     * @param {string} type  - The transaction types - `deposit | withdrawal | transfer`
     * @return {object} - The transaction transaction created
     */
  app.post('/transactions', async (req, res) => {
    const txTypes = ['deposit', 'withdrawal', 'transfer'];
    const {
      transactions: transactionsDb,
      accounts: accountsDb,
    } = req.db.data;

    const {
      accountId,
      type: txType,
      amount,
      destinationAccountId,
    } = req.body;

    if (!_.includes(txTypes, txType)) {
      return res.status(400).json({ message: 'Error: Invalid transaction type' });
    }

    const txAmount = Number(amount);
    if (!txAmount) return res.status(400).json({ message: 'Error: Invalid amount' });

    const account = accountsDb.find((r) => r.id === accountId);

    if (!account) return res.status(404).json({ message: 'Error: Account not found' });

    const transactionRec = {
      id: uuid.generate(),
      createdAt: DateTime.utc().toISO(),
      updatedAt: DateTime.utc().toISO(),
      ...req.body,
    };

    let newBalance;

    // eslint-disable-next-line default-case
    switch (txType) {
      case 'deposit':
        newBalance = account.balance + txAmount;
        break;
      case 'withdrawal':
        newBalance = account.balance - txAmount;

        if (newBalance < 0) newBalance = 0;
        break;
    }

    if (txType === 'transfer') {
      const destAccount = accountsDb.find((r) => r.id === destinationAccountId);
      if (!destAccount) {
        return res.status(404).json({ message: 'Error: Destination account not found' });
      }

      if (accountId === destinationAccountId) {
        return res.status(400).json({ message: 'Error: Source account cannot be the same as the destination account' });
      }

      const txStat = transferTx({
        txAmount,
        account,
        destAccount,
        accountsDb,
      });

      if (!txStat.status) { return res.status(400).json({ message: txStat.msg }); }

      newBalance = txStat.srcBal;

      accountsDb.map((r) => {
        if (r.id === destinationAccountId) {
          _.assign(r, {
            balance: txStat.destBal,
          });
        }
        return r;
      });
    }

    transactionsDb.push(transactionRec);

    accountsDb.map((r) => {
      if (r.id === accountId) {
        _.assign(r, {
          balance: newBalance,
        });
      }
      return r;
    });

    await req.db.write();

    return res.status(200).json(transactionRec);
  });
}
