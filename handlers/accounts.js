import { DateTime } from 'luxon';
import uuid from 'short-uuid';
import _ from 'lodash';

export default function (app) {
  /**
     * Retrieves an account
     * @param {number} id - The account ID
     * @return {object} - The account details including balance
     */
  app.get('/accounts/:id', async (req, res) => {
    const {
      accounts: accountsDb,
    } = req.db.data;

    const accountId = req.params.id;
    const account = accountsDb.find((r) => r.id === accountId);

    if (!account) return res.status(404).json({ message: 'Error: Account does not exist' });

    return res.send(account);
  });

  /**
     * Creates a new account
     * @param {string} userId - The userId of the account that belongs to
     * @param {string} type - Types of account - `savings | checking`
     * @return {object} - The account record created
     */
  app.post('/accounts', async (req, res) => {
    // New account created should have balance:0
    const accountTypes = ['savings', 'checking'];
    const {
      accounts: accountsDb,
      users: usersDb,
    } = req.db.data;

    const accountsRec = {
      id: uuid.generate(),
      createdAt: DateTime.utc().toISO(),
      updatedAt: DateTime.utc().toISO(),
      balance: 0,
      ...req.body,
    };

    if (!_.includes(accountTypes, accountsRec.type)) {
      return res.status(400).json({ message: 'Error: Invalid account type' });
    }

    const user = usersDb.find((r) => r.id === accountsRec.userId);

    if (!user) return res.status(400).json({ message: 'Error: Invalid user for account' });

    const account = accountsDb.push(accountsRec);

    app.log.info({
      id: account.id,
    }, 'Account created');

    await req.db.write();

    return res.status(201).json(accountsRec);
  });
}
